using projeto.pedido.Models.Entities.Base;

namespace projeto.pedido.Models.Entities
{
    public class ProductEntity : BaseEntity
    {
        public string Name { get; set; }


        public override string ToString()
        {
            var s = base.ToString();
            s += Name;

            return s;
        }
    }
}