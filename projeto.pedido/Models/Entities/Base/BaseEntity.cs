using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace projeto.pedido.Models.Entities.Base
{
    public class BaseEntity 
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public bool IsActive { get; set; }

        public BaseEntity()
        {
            Id = -1;
            IsActive = true;
        }

        public BaseEntity(int id)
        {
            Id = id;
            IsActive = true;
        }
        
        public override string ToString()
        {
            return Id + " ";
        }
    }
}