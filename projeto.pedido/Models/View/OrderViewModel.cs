using System.Collections.Generic;
using projeto.pedido.Models.Entities;
using projeto.pedido.Models.View.Base;

namespace projeto.pedido.Models.View
{
    public class OrderViewModel : BaseViewModel<OrderEntity>
    {
        public List<ProductEntity> ProductList { get; set; }
        public List<ClientEntity> ClientList { get; set; }

        public int SelectedProduct { get; set; }
        public int SelectedClient { get; set; }

        public OrderViewModel()
        {
            ProductList = new List<ProductEntity>();
            ClientList = new List<ClientEntity>();

            SelectedProduct = 0;
            SelectedClient = 0;
        }
        
        public override OrderEntity ViewToEntity()
        {
            this.Entity.ProductId = SelectedProduct;
            this.Entity.ClientId = SelectedClient;
            return this.Entity;
        }
    }
}