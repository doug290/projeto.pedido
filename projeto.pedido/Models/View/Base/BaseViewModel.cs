using projeto.pedido.Models.Entities.Base;

namespace projeto.pedido.Models.View.Base
{
    public class BaseViewModel <TEntity> where TEntity : BaseEntity, new()
    {
        public BaseViewModel()
        {
            Entity = new TEntity();
        }
        
        public TEntity Entity { get; set; }

        public virtual TEntity ViewToEntity()
        {
            return Entity;
        }

    }
}