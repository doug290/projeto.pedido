using System.Globalization;

namespace projeto.pedido.Extensions
{
    public static class NumberExtensions
    {
        public static string FormatToCurrency(this double value)
            => FormatToCurrency((decimal) value);

        public static string FormatToCurrency(this decimal value)
        {
            var cultureInfo = new CultureInfo("pt-BR");
            var numberFormatInfo = (NumberFormatInfo) cultureInfo.NumberFormat.Clone();
            numberFormatInfo.CurrencySymbol = "R$ ";
            return string.Format(numberFormatInfo, "{0:C}", value);
        }
    }
}