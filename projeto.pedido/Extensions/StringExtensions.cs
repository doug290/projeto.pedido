using System;

namespace projeto.pedido.Extensions
{
    public static class StringExtensions
    {
        public static bool SearchContains(this string text, string value)
        {
            return SearchContains(text, value, StringComparison.CurrentCultureIgnoreCase);
        }
        
        public static bool SearchContains(this string text, string value, StringComparison stringComparison)
        {
            return text.IndexOf(value, stringComparison) >= 0;
        }
    }
}