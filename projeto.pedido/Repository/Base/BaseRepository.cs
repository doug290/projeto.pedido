using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using projeto.pedido.DbContext;
using projeto.pedido.Models.Entities.Base;

namespace projeto.pedido.Repository.Base
{
    public class BaseRepository<TContext, TModel> where TContext : DbMContext where TModel : BaseEntity
    {

        private readonly TContext _context;
        private readonly DbSet<TModel> _dbSet;

        protected DbSet<TModel> DbSet
        {
            get { return this._dbSet; }
        }

        protected TContext Context
        {
            get { return this._context; }
        }

        public BaseRepository(TContext context, Microsoft.EntityFrameworkCore.DbSet<TModel> dbSet)
        {
            this._dbSet = dbSet;
            this._context = context;
        }
        
        public async Task CreateAsync(TModel item)
        {
            if (item.Id == -1)
            {
                item.Id = 0;
            }
            
            BaseRepository<TContext, TModel> baseRepository = this;
            EntityEntry<TModel> entityEntry = await baseRepository._dbSet.AddAsync(item, new CancellationToken());
            await baseRepository._context.SaveChangesAsync(new CancellationToken());
        }

        public async Task UpdateAsync(TModel item)
        {
            BaseRepository<TContext, TModel> baseRepository = this;
            baseRepository._dbSet.Update(item);
            int num = await baseRepository._context.SaveChangesAsync(new CancellationToken());
        }

        public async Task DeleteAsync(TModel item)
        {
            BaseRepository<TContext, TModel> baseRepository = this;
            baseRepository._dbSet.Remove(item);
            int num = await baseRepository._context.SaveChangesAsync(new CancellationToken());
        }
        
        public async Task<List<TModel>> GetAll()
        {
            return await this._dbSet.Where(w=>w.IsActive).ToListAsync();
        }

        public Task<bool> AnyAsync(Expression<Func<TModel, bool>> predicate)
        {
            return this._dbSet.Where(w=>w.IsActive).AnyAsync<TModel>(predicate, new CancellationToken());
        }

        public Task<TModel> GetAsync(params object[] keyValues)
        {
            return this._dbSet.FindAsync(keyValues);
        }
    }
}