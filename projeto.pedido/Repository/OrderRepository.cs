using Microsoft.EntityFrameworkCore;
using projeto.pedido.DbContext;
using projeto.pedido.Models.Entities;
using projeto.pedido.Repository.Base;

namespace projeto.pedido.Repository
{
    public class OrderRepository : BaseRepository<DbMContext, OrderEntity>
    {
        public OrderRepository(DbMContext context) : base(context, context.Orders)
        {
        }
    }
}