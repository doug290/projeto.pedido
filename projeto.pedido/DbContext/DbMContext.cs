using Microsoft.EntityFrameworkCore;
using projeto.pedido.Models.Entities;

namespace projeto.pedido.DbContext
{
    public class DbMContext: Microsoft.EntityFrameworkCore.DbContext
    {
        public DbMContext(DbContextOptions<DbMContext> options)
            : base(options)
        {
        }

        public DbSet<ClientEntity> Clients { get; set; }

        public DbSet<OrderEntity> Orders { get; set; }

        public DbSet<ProductEntity> Products { get; set; }
        
    }
}
