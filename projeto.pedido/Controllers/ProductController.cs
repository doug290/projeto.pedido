using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using projeto.pedido.Controllers.Base;
using projeto.pedido.Models.Entities;
using projeto.pedido.Models.View.Base;
using projeto.pedido.Repository;

namespace projeto.pedido.Controllers
{
    public class ProductController : BaseController<BaseViewModel<ProductEntity>, ProductRepository, ProductEntity>
    {
        public ProductController(ProductRepository repository) : base(repository)
        {
        }

        protected override List<ProductEntity> Sort(List<ProductEntity> list, string sortColumn, bool isAsc)
        {
            //SORTING
            if (!string.IsNullOrEmpty(sortColumn))
            {
                switch (sortColumn)
                {
                    case "Id":
                        list = isAsc
                            ? list.OrderBy(s => s.Id).ToList()
                            : list.OrderByDescending(s => s.Id).ToList();
                        break;
                    case "Name":
                        list = isAsc
                            ? list.OrderBy(s => s.Name).ToList()
                            : list.OrderByDescending(s => s.Name).ToList();
                        break;
                }
            }

            return list;
        }
    }
}