﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using projeto.pedido.Controllers.Base;
using projeto.pedido.Models;
using projeto.pedido.Models.Entities;
using projeto.pedido.Models.View;
using projeto.pedido.Repository;

namespace projeto.pedido.Controllers
{
    public class OrderController : BaseController<OrderViewModel, OrderRepository, OrderEntity>
    {
        private readonly ProductRepository _productRepository;
        private readonly ClientRepository _clientRepository;

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }

        public OrderController(OrderRepository repository, ProductRepository productRepository,
            ClientRepository clientRepository) : base(repository)
        {
            _productRepository = productRepository;
            _clientRepository = clientRepository;
        }

        protected override async Task CompletViewModel(OrderViewModel viewModel)
        {
            viewModel.ProductList = await _productRepository.GetAll();
            viewModel.ClientList = await _clientRepository.GetAll();
        }

        protected override async Task GetIncludes(OrderEntity entity)
        {
            entity.Product = await _productRepository.GetAsync(entity.ProductId);
            entity.Client = await _clientRepository.GetAsync(entity.ClientId);
        }

        protected override List<OrderEntity> Sort(List<OrderEntity> list, string sortColumn, bool isAsc)
        {
            //SORTING
            if (!string.IsNullOrEmpty(sortColumn))
            {
                switch (sortColumn)
                {
                    case "Id":
                        list = isAsc
                            ? list.OrderBy(s => s.Id).ToList()
                            : list.OrderByDescending(s => s.Id).ToList();
                        break;
                    case "Client.Name":
                        list = isAsc
                            ? list.OrderBy(s => s.Client.Name).ToList()
                            : list.OrderByDescending(s => s.Client.Name).ToList();
                        break;

                    case "Product.Name":
                        list = isAsc
                            ? list.OrderBy(s => s.Product.Name).ToList()
                            : list.OrderByDescending(s => s.Product.Name).ToList();
                        break;

                    case "Total":
                        list = isAsc
                            ? list.OrderBy(s => s.TotalValue).ToList()
                            : list.OrderByDescending(s => s.TotalValue).ToList();
                        break;
                }
            }

            return list;
        }
    }
}