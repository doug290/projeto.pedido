using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using projeto.pedido.Controllers.Base;
using projeto.pedido.DbContext;
using projeto.pedido.Models.Entities;
using projeto.pedido.Models.View.Base;
using projeto.pedido.Repository;
using projeto.pedido.Repository.Base;

namespace projeto.pedido.Controllers
{
    public class ClientController : BaseController<BaseViewModel<ClientEntity>, ClientRepository, ClientEntity>
    {
        public ClientController(ClientRepository repository) : base(repository)
        {
        }

        protected override List<ClientEntity> Sort(List<ClientEntity> list, string sortColumn, bool isAsc)
        {
            //SORTING
            if (!string.IsNullOrEmpty(sortColumn))
            {
                switch (sortColumn)
                {
                    case "Id":
                        list = isAsc
                            ? list.OrderBy(s => s.Id).ToList()
                            : list.OrderByDescending(s => s.Id).ToList();
                        break;
                    case "Name":
                        list = isAsc
                            ? list.OrderBy(s => s.Name).ToList()
                            : list.OrderByDescending(s => s.Name).ToList();
                        break;

                    case "Phone":
                        list = isAsc
                            ? list.OrderBy(s => s.Phone).ToList()
                            : list.OrderByDescending(s => s.Phone).ToList();
                        break;
                }
            }

            return list;
        }
    }
}