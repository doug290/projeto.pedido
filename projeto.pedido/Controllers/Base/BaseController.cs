using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using projeto.pedido.DbContext;
using projeto.pedido.Extensions;
using projeto.pedido.Models;
using projeto.pedido.Models.Entities.Base;
using projeto.pedido.Models.Payload;
using projeto.pedido.Models.Proxy;
using projeto.pedido.Models.View.Base;
using projeto.pedido.Repository.Base;

namespace projeto.pedido.Controllers.Base
{
    public abstract class BaseController<TViewModel, TRepository, TModel> : Controller
        where TRepository : BaseRepository<DbMContext, TModel> where TModel : BaseEntity, new() where TViewModel : BaseViewModel<TModel>, new()
    {
        protected readonly TRepository _repository;

        public BaseController(TRepository repository)
        {
            _repository = repository;
        }
        
        public async Task<IActionResult> Item([FromQuery]int? id)
        {
            TViewModel model = new TViewModel();
            if (id.HasValue)
            {
                model.Entity = await _repository.GetAsync(id.Value);
            }

            await CompletViewModel(model);

            return View(model);
        }

        public  IActionResult Table()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Action(TModel model)
        {
            try
            {
                TModel entity = model;

                if (entity.Id == -1)
                    await _repository.CreateAsync(entity);
                else
                    await _repository.UpdateAsync(entity);

                return Json(new ResultProxy
                {
                    Error = false,
                    Message = "Ação feita com sucesso."
                });
            }
            catch (Exception e)
            {
                return Json(new ResultProxy
                {
                    Error = true,
                    Message = "Houve um erro ao executar a ação"
                });
            }
        }

        protected virtual async Task CompletViewModel(TViewModel viewModel)
        {
        }

        protected virtual async Task GetIncludes(TModel entity)
        {
        }
        
        protected abstract List<TModel> Sort(List<TModel> list, string sortColumn, bool isAsc);

        [HttpPost]
        public async Task<IActionResult> GetTable()
        {
            var payload = GetDataTablePayload();

            var list = await _repository.GetAll();

            foreach (var entity in list)
            {
                await GetIncludes(entity);
            }

            var recordsTotal = list.Count;

            list = Sort(list, payload.SortColumn, payload.IsAsc());

            //SEARCH
            if (!string.IsNullOrEmpty(payload.SearchValue))
                list = list.Where(w =>
                    w.ToString().SearchContains(payload.SearchValue)).ToList();

            var recordsFiltered = list.Count;

            list = list.Skip(payload.Skip).Take(payload.PageSize).ToList();

            //CREATING DATA AND DATATABLE PROXY
            return Json(new DataTableProxy<TModel>
            {
                Draw = payload.Draw,
                RecordsFiltered = recordsFiltered,
                RecordsTotal = recordsTotal,
                Data = list
            });
        }

        public DataTablePayload GetDataTablePayload()
        {
            var payload = new DataTablePayload();

            payload.Draw = HttpContext.Request.Form["draw"].FirstOrDefault();

            // Skip number of Rows count  
            payload.Skip = Request.Form["start"].FirstOrDefault() != null
                ? Convert.ToInt32(Request.Form["start"].FirstOrDefault())
                : 0;

            // Paging Length 10,20  
            payload.PageSize = Request.Form["length"].FirstOrDefault() != null
                ? Convert.ToInt32(Request.Form["length"].FirstOrDefault())
                : 0;

            // Sort Column Name  
            payload.SortColumn = Request
                .Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();

            // Sort Column Direction (asc, desc)  
            payload.SortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();

            // Search Value from (Search box)  
            payload.SearchValue = Request.Form["search[value]"].FirstOrDefault();

            return payload;
        }
    }
}