#pragma checksum "C:\Projetos\projeto.pedido\projeto.pedido\Views\Client\Item.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "f24b0df25612dd8d59d64b1d4f083d5e6a6f97da"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Client_Item), @"mvc.1.0.view", @"/Views/Client/Item.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Client/Item.cshtml", typeof(AspNetCore.Views_Client_Item))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Projetos\projeto.pedido\projeto.pedido\Views\_ViewImports.cshtml"
using projeto.pedido;

#line default
#line hidden
#line 2 "C:\Projetos\projeto.pedido\projeto.pedido\Views\_ViewImports.cshtml"
using projeto.pedido.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"f24b0df25612dd8d59d64b1d4f083d5e6a6f97da", @"/Views/Client/Item.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"5baa280791f5912a32c2c89dfb7e044dc46644ff", @"/Views/_ViewImports.cshtml")]
    public class Views_Client_Item : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<projeto.pedido.Models.View.Base.BaseViewModel<projeto.pedido.Models.Entities.ClientEntity>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("type", "text", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("id", new global::Microsoft.AspNetCore.Html.HtmlString("name"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("id", new global::Microsoft.AspNetCore.Html.HtmlString("phone"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("id", new global::Microsoft.AspNetCore.Html.HtmlString("entity-form"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "post", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("role", new global::Microsoft.AspNetCore.Html.HtmlString("form"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(99, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 3 "C:\Projetos\projeto.pedido\projeto.pedido\Views\Client\Item.cshtml"
  
    ViewData["Title"] = "Cliente";

#line default
#line hidden
            BeginContext(144, 63, true);
            WriteLiteral("\r\n<div class=\"modal-content\">\r\n    <div class=\"modal-header\">\r\n");
            EndContext();
#line 9 "C:\Projetos\projeto.pedido\projeto.pedido\Views\Client\Item.cshtml"
         if (Model.Entity.Id == -1)
        {

#line default
#line hidden
            BeginContext(255, 76, true);
            WriteLiteral("            <h5 class=\"modal-title\" id=\"largeModalLabel\">Novo Cliente</h5>\r\n");
            EndContext();
#line 12 "C:\Projetos\projeto.pedido\projeto.pedido\Views\Client\Item.cshtml"
        }
        else
        {

#line default
#line hidden
            BeginContext(367, 66, true);
            WriteLiteral("            <h5 class=\"modal-title\" id=\"largeModalLabel\">Cliente: ");
            EndContext();
            BeginContext(434, 17, false);
#line 15 "C:\Projetos\projeto.pedido\projeto.pedido\Views\Client\Item.cshtml"
                                                             Write(Model.Entity.Name);

#line default
#line hidden
            EndContext();
            BeginContext(451, 7, true);
            WriteLiteral("</h5>\r\n");
            EndContext();
#line 16 "C:\Projetos\projeto.pedido\projeto.pedido\Views\Client\Item.cshtml"
        }

#line default
#line hidden
            BeginContext(469, 18, true);
            WriteLiteral("    </div>\r\n\r\n    ");
            EndContext();
            BeginContext(487, 773, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "81ce845a31d04ed785537c8352f33e55", async() => {
                BeginContext(536, 80, true);
                WriteLiteral("\r\n        <div class=\"modal-body\">\r\n            <input type=\"text\" id=\"entityId\"");
                EndContext();
                BeginWriteAttribute("value", " value=\"", 616, "\"", 640, 1);
#line 21 "C:\Projetos\projeto.pedido\projeto.pedido\Views\Client\Item.cshtml"
WriteAttributeValue("", 624, Model.Entity.Id, 624, 16, false);

#line default
#line hidden
                EndWriteAttribute();
                BeginContext(641, 94, true);
                WriteLiteral(" hidden=\"true\"/>\r\n\r\n            <div >\r\n                <label>Nome:</label>\r\n                ");
                EndContext();
                BeginContext(735, 52, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("input", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "ad5c2cb51ed5489d8a48817b4cdc151e", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper);
                __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.InputTypeName = (string)__tagHelperAttribute_0.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
#line 25 "C:\Projetos\projeto.pedido\projeto.pedido\Views\Client\Item.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => __model.Entity.Name);

#line default
#line hidden
                __tagHelperExecutionContext.AddTagHelperAttribute("asp-for", __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(787, 101, true);
                WriteLiteral("\r\n            </div>\r\n\r\n            <div>\r\n                <label>Telefone:</label>\r\n                ");
                EndContext();
                BeginContext(888, 54, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("input", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "bd83fa6a719a4faab5628136cdca0ce1", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper);
                __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.InputTypeName = (string)__tagHelperAttribute_0.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
#line 30 "C:\Projetos\projeto.pedido\projeto.pedido\Views\Client\Item.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => __model.Entity.Phone);

#line default
#line hidden
                __tagHelperExecutionContext.AddTagHelperAttribute("asp-for", __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(942, 148, true);
                WriteLiteral("\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"modal-footer\">\r\n            <a class=\"btn btn-secondary btn-rounded\" style=\"width: 15%;\"");
                EndContext();
                BeginWriteAttribute("href", " href=\"", 1090, "\"", 1117, 1);
#line 35 "C:\Projetos\projeto.pedido\projeto.pedido\Views\Client\Item.cshtml"
WriteAttributeValue("", 1097, Url.Action("Table"), 1097, 20, false);

#line default
#line hidden
                EndWriteAttribute();
                BeginContext(1118, 135, true);
                WriteLiteral(">Voltar</a>\r\n            <button type=\"submit\" id=\"btnSave\" class=\"btn btn-primary btn-rounded\">Salvar</button>\r\n        </div>\r\n\r\n    ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_4.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_4);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1260, 14, true);
            WriteLiteral("\r\n\r\n</div>\r\n\r\n");
            EndContext();
            DefineSection("Scripts", async() => {
                BeginContext(1293, 1009, true);
                WriteLiteral(@"
    <script type=""text/javascript"">

        jQuery(document).ready(function($) {

            $('#entity-form').submit(function() {

                $(""#btnSave"").prop(""disabled"", true);

                var name = $('#name').val();
                if (name === '') {
                    alert(""É necessário preencher o nome."");
                    $(""#btnSave"").prop(""disabled"", false);
                    return false;
                }

                var phone = $('#phone').val();
                if (phone === '') {
                    alert(""É necessário preencher o telefone."");
                    $(""#btnSave"").prop(""disabled"", false);
                    return false;
                }

                var form = {
                    'id': $('#entityId').val(),
                    'name': $('#name').val(),
                    'phone': $('#phone').val()
                };

                $.ajax({
                    type: ""POST"",
                    url: '");
                EndContext();
                BeginContext(2303, 20, false);
#line 75 "C:\Projetos\projeto.pedido\projeto.pedido\Views\Client\Item.cshtml"
                     Write(Url.Action("Action"));

#line default
#line hidden
                EndContext();
                BeginContext(2323, 470, true);
                WriteLiteral(@"',
                    data: {
                        'model': form
                    },
                    success: function(data) {
                        if (data.error) {
                            alert(""Erro! - "" + data.message);
                            $(""#btnSave"").prop(""disabled"", false);
                        } else {
                            alert(""Sucesso! - "" + data.message);
                            $(location).attr('href','");
                EndContext();
                BeginContext(2794, 19, false);
#line 85 "C:\Projetos\projeto.pedido\projeto.pedido\Views\Client\Item.cshtml"
                                                Write(Url.Action("Table"));

#line default
#line hidden
                EndContext();
                BeginContext(2813, 380, true);
                WriteLiteral(@"');
                        }

                    },
                    error: function(data) {
                        alert(""Houve um erro."");
                        $(""#btnSave"").prop(""disabled"", false);
                    },
                    dataType: ""json""
                });

                return false;
            });
        });

    </script>
");
                EndContext();
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<projeto.pedido.Models.View.Base.BaseViewModel<projeto.pedido.Models.Entities.ClientEntity>> Html { get; private set; }
    }
}
#pragma warning restore 1591
