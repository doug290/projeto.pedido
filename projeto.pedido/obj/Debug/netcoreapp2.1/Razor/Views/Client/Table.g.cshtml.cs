#pragma checksum "C:\Projetos\projeto.pedido\projeto.pedido\Views\Client\Table.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "62029017788711cc55323de5401e2fe97d1f9d05"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Client_Table), @"mvc.1.0.view", @"/Views/Client/Table.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Client/Table.cshtml", typeof(AspNetCore.Views_Client_Table))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Projetos\projeto.pedido\projeto.pedido\Views\_ViewImports.cshtml"
using projeto.pedido;

#line default
#line hidden
#line 2 "C:\Projetos\projeto.pedido\projeto.pedido\Views\_ViewImports.cshtml"
using projeto.pedido.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"62029017788711cc55323de5401e2fe97d1f9d05", @"/Views/Client/Table.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"5baa280791f5912a32c2c89dfb7e044dc46644ff", @"/Views/_ViewImports.cshtml")]
    public class Views_Client_Table : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 1 "C:\Projetos\projeto.pedido\projeto.pedido\Views\Client\Table.cshtml"
  
    ViewData["Title"] = "Lista de Pedidos";

#line default
#line hidden
            BeginContext(52, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            DefineSection("Scripts", async() => {
                BeginContext(73, 1392, true);
                WriteLiteral(@"
    <script type=""text/javascript"">
        jQuery(document).ready(function($) {
            function loadTable() {

                $(""#table"").dataTable({
                    ""processing"": true,
                    ""serverSide"": true,
                    ""orderMulti"": false,
                    ""language"": language,
                    ""order"": [[0, ""asc""]],
                    ""ajax"": {
                        ""url"": ""client/getTable"",
                        ""type"": ""POST"",
                        ""dataType"": ""json""
                    },
                    ""columnDefs"":
                    [
                        {
                            ""visible"": false,
                            ""searchable"": false
                        }
                    ],
                    ""columns"": [
                        { ""data"": ""id"", ""name"": ""Id"", ""width"": ""auto"" },
                        { ""data"": ""name"", ""name"": ""Name"", ""width"": ""auto"" },
                        { ""data"": ""phone");
                WriteLiteral(@""", ""name"": ""Phone"", ""width"": ""auto"", ""className"": ""text-center"" },
                        {
                            ""width"": ""8%"",
                            ""orderable"": false,
                            ""className"": ""text-center"",
                            ""render"": function(data, type, full, meta) {
                                return '<a href=""");
                EndContext();
                BeginContext(1466, 18, false);
#line 38 "C:\Projetos\projeto.pedido\projeto.pedido\Views\Client\Table.cshtml"
                                            Write(Url.Action("Item"));

#line default
#line hidden
                EndContext();
                BeginContext(1484, 333, true);
                WriteLiteral(@"?id=' +
                                    full.id +
                                    '"" class=""edit btn btn-info btn-rounded fa fa-edit ""></a>';
                            }
                        }
                    ]
                });


            };

            loadTable();

        });
    </script>
");
                EndContext();
            }
            );
            BeginContext(1820, 499, true);
            WriteLiteral(@"
<div class=""modal-content"">
    <div class=""modal-header"">
        <h5 class=""modal-title"" id=""largeModalLabel"">Clientes</h5>
    </div>
    <div class=""modal-body"">
        <div class=""card"">
            <div class=""card-header"">
                <div class=""col-md-10"">
                    <strong>Lista de Cliente</strong>
                </div>
                <div class=""col-md-2"" style=""text-align: right"">
                    <a class=""btn btn-info btn-rounded fa fa-plus-circle""");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 2319, "\"", 2345, 1);
#line 66 "C:\Projetos\projeto.pedido\projeto.pedido\Views\Client\Table.cshtml"
WriteAttributeValue("", 2326, Url.Action("Item"), 2326, 19, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(2346, 536, true);
            WriteLiteral(@"> Novo Cliente </a>
                </div>
            </div>
            <div class=""card-body"">
                <table id=""table"" class=""table table-striped table-bordered"">
                    <thead>
                    <tr>
                        <th>Código</th>
                        <th>Nome</th>
                        <th>Telefone</th>
                        <th>Ações</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
