#pragma checksum "C:\Projetos\projeto.pedido\projeto.pedido\Views\Order\Item.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "a5b94ab02d3c37bb91eaa8438a8c769bcd791739"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Order_Item), @"mvc.1.0.view", @"/Views/Order/Item.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Order/Item.cshtml", typeof(AspNetCore.Views_Order_Item))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Projetos\projeto.pedido\projeto.pedido\Views\_ViewImports.cshtml"
using projeto.pedido;

#line default
#line hidden
#line 2 "C:\Projetos\projeto.pedido\projeto.pedido\Views\_ViewImports.cshtml"
using projeto.pedido.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"a5b94ab02d3c37bb91eaa8438a8c769bcd791739", @"/Views/Order/Item.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"5baa280791f5912a32c2c89dfb7e044dc46644ff", @"/Views/_ViewImports.cshtml")]
    public class Views_Order_Item : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<projeto.pedido.Models.View.OrderViewModel>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("type", new global::Microsoft.AspNetCore.Html.HtmlString("text"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("id", new global::Microsoft.AspNetCore.Html.HtmlString("selectClientId"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("standardSelect form-drop form-control"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("id", new global::Microsoft.AspNetCore.Html.HtmlString("selectProductId"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("type", "text", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("id", new global::Microsoft.AspNetCore.Html.HtmlString("totalValue"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_6 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("form-control input-currency"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_7 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("id", new global::Microsoft.AspNetCore.Html.HtmlString("entity-form"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_8 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "post", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_9 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("role", new global::Microsoft.AspNetCore.Html.HtmlString("form"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.SelectTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(50, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 3 "C:\Projetos\projeto.pedido\projeto.pedido\Views\Order\Item.cshtml"
  
    ViewData["Title"] = "Pedido";

#line default
#line hidden
            BeginContext(94, 63, true);
            WriteLiteral("\r\n<div class=\"modal-content\">\r\n    <div class=\"modal-header\">\r\n");
            EndContext();
#line 9 "C:\Projetos\projeto.pedido\projeto.pedido\Views\Order\Item.cshtml"
         if (Model.Entity.Id == -1)
        {

#line default
#line hidden
            BeginContext(205, 75, true);
            WriteLiteral("            <h5 class=\"modal-title\" id=\"largeModalLabel\">Novo Pedido</h5>\r\n");
            EndContext();
#line 12 "C:\Projetos\projeto.pedido\projeto.pedido\Views\Order\Item.cshtml"
        }
        else
        {

#line default
#line hidden
            BeginContext(316, 65, true);
            WriteLiteral("            <h5 class=\"modal-title\" id=\"largeModalLabel\">Pedido: ");
            EndContext();
            BeginContext(382, 15, false);
#line 15 "C:\Projetos\projeto.pedido\projeto.pedido\Views\Order\Item.cshtml"
                                                            Write(Model.Entity.Id);

#line default
#line hidden
            EndContext();
            BeginContext(397, 7, true);
            WriteLiteral("</h5>\r\n");
            EndContext();
#line 16 "C:\Projetos\projeto.pedido\projeto.pedido\Views\Order\Item.cshtml"
        }

#line default
#line hidden
            BeginContext(415, 18, true);
            WriteLiteral("    </div>\r\n\r\n    ");
            EndContext();
            BeginContext(433, 1213, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "ba7a1749a1894b54976ffa032fbd1f17", async() => {
                BeginContext(482, 80, true);
                WriteLiteral("\r\n        <div class=\"modal-body\">\r\n            <input type=\"text\" id=\"entityId\"");
                EndContext();
                BeginWriteAttribute("value", " value=\"", 562, "\"", 586, 1);
#line 21 "C:\Projetos\projeto.pedido\projeto.pedido\Views\Order\Item.cshtml"
WriteAttributeValue("", 570, Model.Entity.Id, 570, 16, false);

#line default
#line hidden
                EndWriteAttribute();
                BeginContext(587, 96, true);
                WriteLiteral(" hidden=\"true\"/>\r\n\r\n            <div>\r\n                <label>Cliente:</label>\r\n                ");
                EndContext();
                BeginContext(683, 184, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("select", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "e434a376d9c24fcc8691f7c133eb43ee", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.SelectTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
#line 25 "C:\Projetos\projeto.pedido\projeto.pedido\Views\Order\Item.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => __model.Entity.Client.Id);

#line default
#line hidden
                __tagHelperExecutionContext.AddTagHelperAttribute("asp-for", __Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
#line 25 "C:\Projetos\projeto.pedido\projeto.pedido\Views\Order\Item.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper.Items = (new SelectList(Model.ClientList, "Id", "Name"));

#line default
#line hidden
                __tagHelperExecutionContext.AddTagHelperAttribute("asp-items", __Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper.Items, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(867, 59, true);
                WriteLiteral("\r\n                <label>Produto:</label>\r\n                ");
                EndContext();
                BeginContext(926, 187, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("select", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "5c74eef721c941ec996f9b3d7cd8c77c", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.SelectTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
#line 27 "C:\Projetos\projeto.pedido\projeto.pedido\Views\Order\Item.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => __model.Entity.Product.Id);

#line default
#line hidden
                __tagHelperExecutionContext.AddTagHelperAttribute("asp-for", __Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
#line 27 "C:\Projetos\projeto.pedido\projeto.pedido\Views\Order\Item.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper.Items = (new SelectList(Model.ProductList, "Id", "Name"));

#line default
#line hidden
                __tagHelperExecutionContext.AddTagHelperAttribute("asp-items", __Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper.Items, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(1113, 89, true);
                WriteLiteral("\r\n                <div >\r\n                    <label>Valor:</label>\r\n                    ");
                EndContext();
                BeginContext(1202, 100, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("input", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "c7fdc7bdb3b84ec8a61d8eb900f288e9", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper);
                __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.InputTypeName = (string)__tagHelperAttribute_4.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_4);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_6);
#line 30 "C:\Projetos\projeto.pedido\projeto.pedido\Views\Order\Item.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => __model.Entity.TotalValue);

#line default
#line hidden
                __tagHelperExecutionContext.AddTagHelperAttribute("asp-for", __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(1302, 174, true);
                WriteLiteral("\r\n                </div>\r\n            </div>\r\n\r\n        </div>\r\n\r\n        <div class=\"modal-footer\">\r\n            <a class=\"btn btn-secondary btn-rounded\" style=\"width: 15%;\"");
                EndContext();
                BeginWriteAttribute("href", " href=\"", 1476, "\"", 1503, 1);
#line 37 "C:\Projetos\projeto.pedido\projeto.pedido\Views\Order\Item.cshtml"
WriteAttributeValue("", 1483, Url.Action("Table"), 1483, 20, false);

#line default
#line hidden
                EndWriteAttribute();
                BeginContext(1504, 135, true);
                WriteLiteral(">Voltar</a>\r\n            <button type=\"submit\" id=\"btnSave\" class=\"btn btn-primary btn-rounded\">Salvar</button>\r\n        </div>\r\n\r\n    ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_7);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_8.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_8);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_9);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1646, 14, true);
            WriteLiteral("\r\n\r\n</div>\r\n\r\n");
            EndContext();
            DefineSection("Scripts", async() => {
                BeginContext(1679, 3007, true);
                WriteLiteral(@"
    <script type=""text/javascript"">

        jQuery(document).ready(function($) {

            function getValue(ref) {
                var valueText = jQuery(ref).val();
                valueText = valueText.replace(/\./g, """");
                valueText = valueText.replace("","", ""."");
                valueText = valueText.replace(""R$ "", """");
                var value = parseFloat(valueText);
                return parseInt(value * 100);
            }; 

            function UpdateMoneyMask(ref) {
                jQuery(ref).maskMoney({
                    prefix: ""R$ "",
                    decimal: "","",
                    thousands: ""."",
                    allowZero: true,
                    affixesStay: false
                });
            };

            function checkDecimal(ref) {
                var text = jQuery(ref).val();
                if (text.indexOf(""."") !== -1) {
                    var decimal = text.split(""."")[1];
                    if (decimal.length === 1) {");
                WriteLiteral(@"
                        text += ""0"";
                    }
                    text = text.replace(""."", """");
                    text = text.replace(/\,/g, """");
                } else {
                    text = getValue(ref);
                }
                jQuery(ref).val(text);
            }

            function ApplyMaskMoney(ref) {
                jQuery(ref).maskMoney('mask')
            };

            checkDecimal(""#totalValue"");
            UpdateMoneyMask("".input-currency"");
            ApplyMaskMoney("".input-currency"");

            $('#entity-form').submit(function() {

                $(""#btnSave"").prop(""disabled"", true);

                var clientId = $('#selectClientId').val();
                if (clientId === 0 || clientId === null || clientId === '') {
                    alert(""É necessário selecionar um cliente."");
                    $(""#btnSave"").prop(""disabled"", false);
                    return false;
                }

                var productId ");
                WriteLiteral(@"= $('#selectProductId').val();
                if (productId === 0 || productId === null || productId === '') {
                    alert(""É necessário selecionar um produto."");
                    $(""#btnSave"").prop(""disabled"", false);
                    return false;
                }

                var totalValue = getValue(""#totalValue"") / 100.00;
                if (totalValue <= 0) {
                    alert(""É necessário colocar um valor maior que R$ 0,00."");
                    $(""#btnSave"").prop(""disabled"", false);
                    return false;
                }

                var form = {
                    'id': $('#entityId').val(),
                    'ClientId': $(""#selectClientId"").val(),
                    'ProductId': $(""#selectProductId"").val(),
                    'TotalValue': totalValue
                };

                $.ajax({
                    type: ""POST"",
                    url: '");
                EndContext();
                BeginContext(4687, 20, false);
#line 127 "C:\Projetos\projeto.pedido\projeto.pedido\Views\Order\Item.cshtml"
                     Write(Url.Action("Action"));

#line default
#line hidden
                EndContext();
                BeginContext(4707, 471, true);
                WriteLiteral(@"',
                    data: {
                        'model': form
                    },
                    success: function(data) {
                        if (data.error) {
                            alert(""Erro! - "" + data.message);
                            $(""#btnSave"").prop(""disabled"", false);
                        } else {
                            alert(""Sucesso! - "" + data.message);
                            $(location).attr('href', '");
                EndContext();
                BeginContext(5179, 19, false);
#line 137 "C:\Projetos\projeto.pedido\projeto.pedido\Views\Order\Item.cshtml"
                                                 Write(Url.Action("Table"));

#line default
#line hidden
                EndContext();
                BeginContext(5198, 380, true);
                WriteLiteral(@"');
                        }

                    },
                    error: function(data) {
                        alert(""Houve um erro."");
                        $(""#btnSave"").prop(""disabled"", false);
                    },
                    dataType: ""json""
                });

                return false;
            });
        });

    </script>
");
                EndContext();
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<projeto.pedido.Models.View.OrderViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
